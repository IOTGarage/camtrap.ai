import argparse
import sys
import pandas as pd
import argparse
from tools.classifier import CamTrapClassifier

def classfier(args):
  #! /usr/bin/env python
  """ Classify images using a trained model

    Parameters
    ------------
    images_dir:
        - path to root directory that must contain 1 or more directories with
          images

    path_to_model:
        - path to model file
        - string

    model_cfg_json:
        - path to json with model config
        - Json-string with keys: "class_mapper" & "pre_processing"

    export_dir
        - path to directory to which to write the predictions

    export_fname:
        - name of output file  - a csv file (default: predictions.csv)

    batch_size:
        - number of images to process at a time (default 256)


    Usage example:

    python predict.py \
    -images_dir /my_data/new_images/ \
    -path_to_model /my_data/save/ss/ss_species_51_201708072308.hdf5 \
    -model_cfg_json /my_data/save/ss/ss_species_51_201708072308_cfg.json \
    -export_dir /my_data/save/ss/
  """
  

  if __name__ == '__main__':


      classifier = CamTrapClassifier(
                      path_to_model=args.path_to_model,
                      model_cfg_json=args.model_cfg_json)

      classifier.predict_path(
          path=args.images_dir,
          output_path=args.export_dir,
          output_file_name=args.export_fname,
          batch_size=256)


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images_dir",    required=True,    help="dir where the images are present")
ap.add_argument("-p", "--path_to_model", required=True,    help= "path of the tf marco model")
ap.add_argument("-j","--model_cfg_json", required=True,    help= "path to the json file of the marco model")
ap.add_argument("-e","--export_dir",     required=True,    help= "path where the csv file should be stored")
ap.add_argument("-en","--export_fname",  required=True,    help= "name of the csv file")
args = ap.parse_args()

classfier(args)
print('marco et al ran successfully without any errors')