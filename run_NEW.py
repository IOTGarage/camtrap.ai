import os
import argparse
import subprocess
import sys

# Create the parser
ap = argparse.ArgumentParser()
# Add the model_name argument
ap.add_argument("-m", "--model_name", required=True, help="Choosysteme a model name")
# Add the operation_system argument
ap.add_argument("-os", "--operation_system", required=True, choices=["w", "l"], help="Operating system you are using, 'w' for Windows, 'l' for Linux")
# Parse the arguments
args = ap.parse_args()



#######################################################
# Mega-Detector
#######################################################

def megadet_fun():
  run = path_to_md_py+" --images_dir "+images_dir+" --path_to_model "+path_to_md_model+" --folder "+md_folder+" --output_path "+output_path+" --output_images "+images_output_dir
  #run = o.path.join("mega_detector", "mega_det.py")
  #print(dirroot)
  print('Running Mega Detector')
  os.system(' python '+run)


#######################################################
# Marco
#######################################################
def marco_fun():
  run = path_to_marco_py+" --images_dir "+images_dir+ " --path_to_model "+path_to_marco+" --model_cfg_json "+path_to_marco_json+" --export_dir "+output_path+" --export_fname /marco.csv"
  print('python '+run)
  print('running willi,marco et.al')
  os.system('python '+run)

def marco_sub_fun():
  run = path_to_marco_sub_py+" --images_dir "+images_dir+  " --path_to_model "+path_to_marco_sub+" --model_cfg_json "+path_to_marco_sub_json+" --export_dir "+output_path+" --export_fname /marco_sub.csv"
  print('running willi,marco et.al sub')
  os.system('python '+run)

#######################################################
# Google Vision
#######################################################


def google_cloud_fun():
  run = path_to_google+" --images_dir "+images_dir+" --path_to_credentials_json "+path_to_credentials_json+" --output_path "+output_path
  print('running GOOGLG VISION AI')
  os.system('python '+run)

#######################################################
# Evolving AI
#######################################################

def evolving_ai_lab_p1_fun():
  run=evolving_lab_ai_phase1+' --data_info '+path_to_csv_p1+' --log_dir '+path_to_log_dir_1+' --save_predictions '+output_path+'/evloving_ai_phase-1.csv'+' --path_prefix '+images_dir+'pics-2'
  print("RUNNING EVOLVING AI LAB -1")
  os.system('python '+run)


def evolving_ai_lab_p2_fun():
  run=evolving_lab_ai_phase2+' --data_info '+path_to_csv_p2+' --log_dir '+path_to_log_dir_2+' --save_predictions '+output_path+'/evloving_ai_phase-2.csv'+' --path_prefix '+images_dir+'pics-2'
  print("RUNNING EVOLVING AI LAB-2")
  os.system('python '+run)

#######################################################
# MS-Species
#######################################################


def MS_species_fun(model_number): #gets the model number
    run=path_to_MS_species_py +" --images_dir "+images_dir+" --output_path "+output_path+" --path_to_MS_repo "+path_to_MS_repo+" --model_number "+str(model_number)
    print("RUNNING MS_species \nrunning "+ str(model_number) +" models")
    os.system('python '+run)

#######################################################
# Mega_detector:
#######################################################
if args.model_name == "megadet": #==True:
  if args.operation_system == "l":
    ## Linux path
    images_dir = "./inputs"
    output_path = './mega_detector/outputs'
    images_output_dir = './mega_detector/outputs/imgs_dir'
    path_to_md_py ='./mega_detector/mega_det_NEW.py'
    path_to_md_model ='./mega_detector/megadetector_v4_1_0.pb' 
    md_folder = './mega_detector' 

  if args.operation_system == "w":
    # Windows paths
    images_dir = ".\\inputs"
    output_path = '.\\mega_detector\\outputs'
    images_output_dir = '.\\mega_detector\\outputs\\imgs_dir'
    path_to_md_py = '.\\mega_detector\\mega_det_NEW.py'
    path_to_md_model = '.\\mega_detector\\megadetector_v4_1_0.pb'
    md_folder = '.\\mega_detector'

#######################################################
# Marco model:
#######################################################
# Path to the outputs folder
if args.model_name=="marco": # == True:
  if args.operation_system == "l":  # Linux paths
      images_dir = "./inputs"
      output_path = './marco/outputs'
      path_to_marco_model = './marco'
      path_to_marco_py = './marco/marco.py'
      path_to_marco_sub_py = './marco/marco_sub.py'
      path_to_marco = './marco/models/camera_catalogue/cc_blank_vehicle_species_v2_201708200608.hdf5'
      path_to_marco_json = './marco/models/camera_catalogue/cc_blank_vehicle_species_v2_201708200608_cfg.json'
      path_to_marco_sub = './marco/models/camera_catalogue/cc_species_v2_201708210308.hdf5'
      path_to_marco_sub_json = './marco/models/camera_catalogue/cc_species_v2_201708210308_cfg.json'
  if args.operation_system == "w":  # Windows paths
      images_dir = ".\\inputs"
      output_path = '.\\marco\\outputs'
      path_to_marco_model = '.\\marco'
      path_to_marco_py = '.\\marco\\marco.py'
      path_to_marco_sub_py = '.\\marco\\marco_sub.py'
      path_to_marco = '.\\marco\\models\\camera_catalogue\\cc_blank_vehicle_species_v2_201708200608.hdf5'
      path_to_marco_json = '.\\marco\\models\\camera_catalogue\\cc_blank_vehicle_species_v2_201708200608_cfg.json'
      path_to_marco_sub = '.\\marco\\models\\camera_catalogue\\cc_species_v2_201708210308.hdf5'
      path_to_marco_sub_json = '.\\marco\\models\\camera_catalogue\\cc_species_v2_201708210308_cfg.json'

#######################################################
# Marco-sub model:
#######################################################
# Path to the outputs folder
if args.model_name=="marco_sub": # == True:
  if args.operation_system == "l":  # Linux paths
      images_dir = "./inputs"
      output_path = './marco/outputs'
      path_to_marco_model = './marco'
      path_to_marco_sub_py = './marco/marco_sub.py'
      path_to_marco_sub = './marco/models/camera_catalogue/cc_species_v2_201708210308.hdf5'
      path_to_marco_sub_json = './marco/models/camera_catalogue/cc_species_v2_201708210308_cfg.json'
  if args.operation_system == "w":  # Windows paths
      images_dir = ".\\inputs"
      output_path = '.\\marco\\outputs'
      path_to_marco_model = '.\\marco'
      path_to_marco_sub_py = '.\\marco\\marco_sub.py'
      path_to_marco_sub = '.\\marco\\models\\camera_catalogue\\cc_species_v2_201708210308.hdf5'
      path_to_marco_sub_json = '.\\marco\\models\\camera_catalogue\\cc_species_v2_201708210308_cfg.json'

#######################################################
# Google vision AI
#######################################################

if args.model_name == "google_cloud": #== True:
  if args.operation_system == "l":  # Linux paths
      images_dir = "./inputs/sub/*"
      output_path = "./outputs"
      path_to_google = "./google_vision/google_vision.py"
      path_to_credentials_json = "./google_vision/gdet-323417-74e7dbedf0ef.json"
  if args.operation_system == "w":  # Windows paths
      images_dir = ".\\inputs\\sub\\*"
      output_path = ".\\outputs"
      path_to_google = ".\\google_vision\\google_vision.py"
      path_to_credentials_json = ".\\google_vision\\gdet-323417-74e7dbedf0ef.json"

  
#######################################################
# Evolving_1 
#######################################################
if args.model_name == "evolving_ai_lab_p1":

  if args.operation_system == "l":  # Linux paths
      images_dir = "./inputs"
      output_path = './evolving_ai_lab/outputs'
      evolving_lab_ai_phase1 = './evolving_ai_lab/Evolving-lab-ai-p1.py'
      path_to_log_dir_1 = './evolving_ai_lab/saved_models/phase-1'
      path_to_csv_p1 = './evolving_ai_lab/val.txt'



  if args.operation_system == "w":  # Windows paths
      images_dir = ".\\inputs"
      output_path = '.\\evolving_ai_lab\\outputs'
      evolving_lab_ai_phase1 = '.\\evolving_ai_lab\\Evolving-lab-ai-p1.py'
      path_to_log_dir_1 = '.\\evolving_ai_lab\\saved_models\\phase-1'
      path_to_csv_p1 = '.\\evolving_ai_lab\\val.txt'


  os.system('python ./evolving_ai_lab/input_path_generator.py' )
  print("################################################")
  print("val.txt is generated including all images paths......")
  print("################################################")


########################################################
# Evolving_2
#######################################################
if args.model_name == "evolving_ai_lab_p2":

  if args.operation_system == "l":  # Linux paths
      images_dir = "./inputs"
      output_path = './evolving_ai_lab/outputs'
      evolving_lab_ai_phase2 = './evolving_ai_lab/Evolving-lab-ai-p2.py'
      path_to_log_dir_2 = './evolving_ai_lab/saved_models/phase-2'
      path_to_csv_p2 = './evolving_ai_lab/val3.txt'


  if args.operation_system == "w":  # Windows paths
      images_dir = ".\\inputs"
      output_path = '.\\evolving_ai_lab\\outputs'
      evolving_lab_ai_phase2 = '.\\evolving_ai_lab\\Evolving-lab-ai-p2.py'
      path_to_log_dir_2 = '.\\evolving_ai_lab\\saved_models\\phase-2'
      path_to_csv_p2 = '.\\evolving_ai_lab\\val3.txt'

  os.system('python ./evolving_ai_lab/input_path_generator2.py' )
  print("################################################")
  print("val3.txt is generated including all images paths......")
  print("################################################")     

#######################################################
# MS_species
#######################################################
if args.model_name == "MS_species_1" or args.model_name == "MS_species_2" or args.model_name == "MS_species_3":
  if args.operation_system == "l":  # Linux paths
      images_dir = "./inputs"
      path_to_MS_species_py = './MS_species/SpeciesClassification/MS_species.py'
      path_to_MS_repo = './MS_species'
      output_path = './MS_species/SpeciesClassification/outputs'
  if args.operation_system == "w":  # Windows paths
      images_dir = ".\\inputs"
      path_to_MS_species_py = '.\\MS_species\\SpeciesClassification\\MS_species.py'
      path_to_MS_repo = '.\\MS_species'
      output_path = '.\\MS_species\\SpeciesClassification\\outputs'

  print('started running...')
###############################################################
if args.model_name == "megadet":
  try:
    megadet_fun()
    print('DONE')  
  except:
    print("ERROR: something went wrong, \nplease check all the paths are correct, all imports are properly installed and all virtual environments are properly set")

if args.model_name == "google_cloud":
  print('started google')
  try:
    google_cloud_fun() 
    print('DONE')   
  except:
    print("ERROR: something went wrong, \nplease check all the paths are correct, all imports are properly installed and all virtual environments are properly set")

if args.model_name == "marco":
  try:
    marco_fun()
    print('DONE')    
  except:
    print("ERROR: something went wrong, \nplease check all the paths are correct, all imports are properly installed and all virtual environments are properly set")

if args.model_name=="marco_sub":
  try:
    marco_sub_fun()
    print('DONE')    
  except:
    print("ERROR: something went wrong, \nplease check all the paths are correct, all imports are properly installed and all virtual environments are properly set")

if args.model_name == "evolving_ai_lab_p1" :
  try:
    evolving_ai_lab_p1_fun()
    print("DONE")
  except:
    print("ERROR: something went wrong, \nplease check all the paths are correct, all imports are properly installed and all virtual environments are properly set")


if args.model_name == "evolving_ai_lab_p2" :
  try:
    evolving_ai_lab_p2_fun()
    print("DONE")
  except:
    print("ERROR: something went wrong, \nplease check all the paths are correct, all imports are properly installed and all virtual environments are properly set")

if args.model_name == "MS_species_1" or args.model_name == "MS_species_2" or args.model_name == "MS_species_3":
  if args.model_name == "MS_species_1":
    num=1
  elif args.model_name == "MS_species_2":
    num=2
  elif args.model_name == "MS_species_3":
    num=3
  try:
    MS_species_fun(num)
    print('Done')
  except:
    print("ERROR: something went wrong, \nplease check all the paths are correct, all imports are properly installed and all virtual environments are properly set MS")





