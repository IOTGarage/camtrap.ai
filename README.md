# Overview

* **inputs:** Move your input images here.

* **requirements.txt**: The packages needed to run all the models.

* **hub.py**: This file is only needed for the MS_specis models.

* **outputs**: All outputs are saved inside each model directory in the **"outputs"** folder based on the model used.

# **Pre-requisite:** </u>

* &rarr; **Linux OS**

* &rarr; **Python3.6**

* &rarr; **CUDA** 

> **Note**: These models were tested on Linux operating system. So some packages or dependencies might needed if you are using Windows OS.

* Create a conda environment:

 ```
 conda create -n camtrapai python=3.6
 ```

* Activate the created env:

```
conda activate camtrapai
```

* Install requirements.txt:

```
pip3 install -r requirements.txt
```

* Install torch 1.10 from [Click Here](https://pytorch.org/get-started/previous-versions/). You can install for CPU or GPU.

* Install cudatoolkit:

```
conda install cudatoolkit==10.0.130
```

* Install cudnn:

```
conda install cudnn==7.6.5
```

* Check GPU availability:

```
$ nvidia-smi
```

* Check CUDA availability:

```
$ nvcc --version
```

> **Note:** You can create your virtual env in different ways.

# Models
## 1. Mega Detector 

1. Download the model-weigths [Click Here](https://drive.google.com/file/d/1KPeJro2dUDp8IQ2ndUhJzDzgq8OhJjeh/view?usp=drive_link)

2. Move the weights to the mega_detector directory.

3. Run the model:

```
$ python3 run_NEW.py -m megadet -os l
```

> **Note:** Results are located at "./mega_detector/outputs" as a CSV file and mages with bbox in the imgs_dir.

 ## 2. Marco & Marco Sub
 
1. Create a new folder **"models"** and access it: 

```
$ cd marco
$ mkdir models
$ cd models
```

2. Download the folders and move them inside the created **"models"** [Click Here](https://drive.google.com/drive/folders/1lH1DyueX9SEbTZH8NNQkqSBdWrBuUx_f?usp=drive_link)


3. Run the model:

```
# For the binary-classifier:
$ python3 run_NEW.py -m marco -os l

# For the species-classifier:
python3 run_NEW.py -m marco_sub -os l
```

> **Note:** 

* In order to run **marco_sub**, **marco** need to be run first. But you can run **maroc** as a standalone for empty and non-empty classes.

* Results are located at "./marco/outputs" as a CSV file.


## 3. Google Cloud:

1. Create a google cloud account [Click Here](https://console.cloud.google.com)

2. From <img src="/icons/create_project.png" width="200" height="40" title="Just an Icon"> icon choose __NEW PROJECT__ and provide __Project Name__ and __Location__ then __Create__. 

3. A console shell will appear, run ```$ gcloud auth list``` Also, run ```$ gcloud config list project``` to confirm the project Id. 

4. Enable the Vision API by running: ```$ gcloud services enable vision.googleapis.com```.

5. Go back to your account and search for __Service Account__ and access it and __CREATE SERVICE ACCOUNT__.

6. In the __Service account name__ enter a name, with no roles required, keep it empty.

7. Access the created service from previous step >> __KEYS__ tab >> __ADD KEY__ >> Choose __JSON__ 
  
8. Download the JSON-file from previous step, then replace/move to "./google_vision" directory.


9. Run the model:

```
$ python3 run_NEW.py -m google_cloud -os l
```

> **Note:** Results are located at "./google_cloud/outputs" as a CSV file.

## 4. Evolving AI Lab

* **Evolving Lab AI Pahse (1):** For species detection.

* **Evolving Lab AI Phase (2):** For sub-categories & activity identification.
----------------------------------------------------------------

1. Create a new folder **"saved_models"** to save the models and access it: 

```
$ cd evolving_ai_lab
$ mkdir saved_models
$ cd saved_models
```

2. Download the folders through the link below inside the created **"saved_models"** [Click Here](https://drive.google.com/drive/folders/143KUYsu0SkpIEUDF5kVfs_NrLqxxG167?usp=drive_link)


3. Extract the file "deep_learning_for_camera_trap_images.zip".

4. Run the model:

```
# For evolving_ai_lab_1
$ python3 run_NEW.py -m evolving_ai_lab_p1 -os l

# For evolving_ai_lab_2
$ python3 run_NEW.py -m evolving_ai_lab_p2 -os l
```

> **Note:** Results are located at "./evolving_ai_lab/outputs" as a CSV file.

## 5. MS Species ###

1. Replace the python file **hub.py** at *" <your_env_name>/lib/python3.6/site-packages/torch/hub.py"* by the **hub.py** attached in the repo.

> **Note**: Change the env-name in the path, in case you selected a different name.

3. Download the model from <click_here>.

4. Move the downloaded model to "./MS_species/SpeciesClassification".

5. Run the model:

```
$ python3 run_NEW.py -m MS_species_1 -os l
```

> **Note:** Results are located at "./MS_species/outputs" as a CSV file.

# References and Notes:

- __Mega Detector__: A "Pre-trained" model to detect and classify animal species. [(Source Link)](https://github.com/microsoft/CameraTraps/tree/main)

- __Marco__: A "Pre-trained" model to classify animal species. [(Source Link)](https://github.com/marco-willi/camera-trap-classifier-publication-code/tree/master)

- __Evolving Lab AI:__ [(Source Link)](https://github.com/Evolving-AI-Lab/deep_learning_for_camera_trap_images)
  
  - __Evolving Lab 1:__ A "Self-trained model" to classify animal species.
  
  - __Evolving Lab 2:__ A "Self-trained model" to classify animals activities.

- __Google Cloud:__ A "Pre-trained" model to classify animal species. [(Source Link)](https://cloud.google.com/vision?hl=en)

- __MS Species:__ A "Pre-trained" model(s) to classify animals species of different categories and sub-categories. [(Source Link)](https://github.com/microsoft/SpeciesClassification)
