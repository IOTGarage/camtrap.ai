import argparse
import json
import pandas as pd
import os
from PIL import Image, ImageFile, ImageFont, ImageDraw
import numpy as np
from tqdm import tqdm

root = os.getcwd()
dirname = os.path.dirname(root)

classes = {1: "animal", 2: "person", 3: "vehicle"}

if os.path.exists("./outputs/md.json"):
    os.remove("./outputs/md.json")
    print("############ Old <md.json> has been deleted ############")
if os.path.exists("./outputs/mega_det.csv"):
   os.remove("./outputs/mega_det.csv")
   print("############ Old <mega_det.csv> has been deleted ############")

def mega_classify():
  #images_dir = os.path.join(root, "inputs")
  images_dir = args.images_dir
  #output_file_path = os.path.join(root, "mega_detector", "outputs", 'md.json')
  output_file_path = args.output_path +'/md.json'
  #con_path = os.path.join(root, 'mega_detector', 'CameraTraps', 'archive', 'detection', 'run_detector_batch.py')
  #model_path = os.path.join(root, "mega_detector", "megadetector_v4_1_0.pb")
  con_path = args.folder+'/CameraTraps/archive/detection/run_detector_batch.py'
  model_path = args.path_to_model
  run = ' '+con_path+' '+model_path+' '+images_dir+' '+output_file_path+' --recursive'
  print('started running..')
  print(run)
  os.system('python'+run) # type: ignore
  data = json.load(open(output_file_path))
  #below 3 lines are used to create bounding boxes
  #visualization_dir = os.path.join(root, 'mega_detector', "outputs", "imgs_dir")  
  visualization_dir = args.output_images 
  
  df = pd.DataFrame(data['images'])
  df.to_csv()
  
  col=["data","max_detection_conf","detections","count"]
  
  temp=[]
  temp2=[[]]
  i = 0
  count=1
  count2=1

  for x in df['detections']:
    p1=str(x)
    try:
      p=p1.split("}, {")
      count+=1
      for values in p:
        num = len(p)
        temp=[df['file'][i],x[0]['bbox'],values,num]
        temp2.append(temp)
    except:
      print("ERROR DATA NOT FOUND")
    i+=1
  temp2.pop(0)
  csv_final = pd.DataFrame(temp2,columns=col)
  csv_final.to_csv()
  col=["data","max_detection_conf","category","conf","bbox","count"]
  temp=""
  conf=""
  category=""
  bbox=""
  strx=""
  i=0
  j=0
  for data in csv_final['detections']:
    print(data)
    temp=data.split(":")
    for data2 in temp:
        temp2 = data2.split(",")
        for data3 in temp2:
          if(i==1 and data !="[]"):
            category=data3
          if(i==3 and data !="[]"):
            conf=data3
          if(i>=5 and i<=8 and data !="[]"):
            bbox = bbox +data3 +", "
          i+=1
          if(i==9 or data=="[]"):
            i=0
            if "}]," in bbox:
              bbox=bbox[:-4]
            else:
              bbox= bbox[:-2]
            strx= strx + csv_final['data'][j]+ ":" + str(csv_final['max_detection_conf'][j]) +":"+ str(category) +":"+ str(conf) +":"+ bbox +":"+ str(csv_final['count'][j]) +" @"
          #temp=[csv_final['data'][j],csv_final['max_detection_conf'][j],category,conf,bbox,csv_final['count'][j]]
          #temp2.append(temp)
            category =""
            conf=""
            bbox=""
    j+=1
  temp2 = strx.split("@")
  temp=[[]]
  for data in temp2:
    temp.append(data.split(":"))
  temp.pop(0)
  csv_final2 = pd.DataFrame(temp,columns=col)
  #output_csv = os.path.join(root, "mega_detector","outputs",'mega_det.csv')
  output_csv = args.output_path+'/mega_det.csv'
  
  
  csv_final2.to_csv(output_csv, index = False)

  #code for bounding boxes
  data = json.load(open(output_file_path))
  for paths in tqdm(data['images']):
    conf_temp =[]
    box_temp = []
    cat_temp = []
    for box_co in (paths['detections']):
      if box_co['conf']>0.8:
         conf_temp.append(box_co['conf'])
         box_temp.append(box_co['bbox'])
         cat_temp.append(box_co['category'])

    print("################################")    
    print("conf_temp:", conf_temp)
    print("################################")
    print("box_temp:", box_temp)
    print("################################")
    print(cat_temp)

    max_conf = max(conf_temp)
    idx = conf_temp.index(max_conf)
    max_box = box_temp[idx]
    max_cat = int(cat_temp[idx])

    print("################################")
    print("max_conf:", max_conf)
    print("################################")
    print("max_box:", max_box)
    print("################################")
    print(max_cat)
         
        
  
  y = ''
  #img_class =  data['detection_categories'][box_co['category']]
  img_class = max_cat
  # A statement for using path in LINUX:
  if "/" in args.output_path:
    tempname = (paths['file'].split('/')[-1])
    img_type=paths['file'].split('.')[-1]

  # A statement for using path in Windows:
  if "\\" in args.output_path:
    tempname = (paths['file'].split('\\')[-1])
    img_type=paths['file'].split('.')[-1]

  save_path =visualization_dir+'/anno_'+tempname
  try:
    y = Image.open(save_path)
  except:
    y = Image.open(paths['file'])
    
  #bbox_image = draw_bounding_box_on_image(y,box_co['bbox'][0],box_co['bbox'][1],box_co['bbox'][2],box_co['bbox'][3],display_str_list =[str(img_class+', confidence='+str(box_co['conf']*100))])

  bbox_image = draw_bounding_box_on_image(y,max_box[0],max_box[1],max_box[2],max_box[3],display_str_list =[str(classes[img_class]+', confidence='+str(max_conf*100))])

  if bbox_image.mode == 'RGBA':
    bbox_image = bbox_image.convert('RGB')

  bbox_image.save(save_path)



  return "done"


##### Function to draw bounding box####
def draw_bounding_box_on_image(image,
                               ymin,
                               xmin,
                               ymax,
                               xmax,
                               clss=None,
                               thickness=4,
                               expansion=0,
                               display_str_list=(),
                               use_normalized_coordinates=True,
                               label_font_size=16,
                               textalign=0):
    """
    Adds a bounding box to an image.

    Bounding box coordinates can be specified in either absolute (pixel) or
    normalized coordinates by setting the use_normalized_coordinates argument.

    Each string in display_str_list is displayed on a separate line above the
    bounding box in black text on a rectangle filled with the input 'color'.
    If the top of the bounding box extends to the edge of the image, the strings
    are displayed below the bounding box.

    Args:
    image: a PIL.Image object.
    ymin: ymin of bounding box - upper left.
    xmin: xmin of bounding box.
    ymax: ymax of bounding box.
    xmax: xmax of bounding box.
    clss: str, the class of the object in this bounding box - will be cast to an int.
    thickness: line thickness. Default value is 4.
    expansion: number of pixels to expand bounding boxes on each side.  Default is 0.
    display_str_list: list of strings to display in box
        (each to be shown on its own line).
        use_normalized_coordinates: If True (default), treat coordinates
        ymin, xmin, ymax, xmax as relative to the image.  Otherwise treat
        coordinates as absolute.
    label_font_size: font size to attempt to load arial.ttf with
    """

    
    #Setting default colors for bounding boxes
    DEFAULT_COLORS = [
        'AliceBlue', 'Red', 'RoyalBlue', 'Gold', 'Chartreuse', 'Aqua', 'Azure',
        'Beige', 'Bisque', 'BlanchedAlmond', 'BlueViolet', 'BurlyWood', 'CadetBlue',
        'AntiqueWhite', 'Chocolate', 'Coral', 'CornflowerBlue', 'Cornsilk', 'Crimson',
        'Cyan', 'DarkCyan', 'DarkGoldenRod', 'DarkGrey', 'DarkKhaki', 'DarkOrange',
        'DarkOrchid', 'DarkSalmon', 'DarkSeaGreen', 'DarkTurquoise', 'DarkViolet',
        'DeepPink', 'DeepSkyBlue', 'DodgerBlue', 'FireBrick', 'FloralWhite',
        'ForestGreen', 'Fuchsia', 'Gainsboro', 'GhostWhite', 'GoldenRod',
        'Salmon', 'Tan', 'HoneyDew', 'HotPink', 'IndianRed', 'Ivory', 'Khaki',
        'Lavender', 'LavenderBlush', 'LawnGreen', 'LemonChiffon', 'LightBlue',
        'LightCoral', 'LightCyan', 'LightGoldenRodYellow', 'LightGray', 'LightGrey',
        'LightGreen', 'LightPink', 'LightSalmon', 'LightSeaGreen', 'LightSkyBlue',
        'LightSlateGray', 'LightSlateGrey', 'LightSteelBlue', 'LightYellow', 'Lime',
        'LimeGreen', 'Linen', 'Magenta', 'MediumAquaMarine', 'MediumOrchid',
        'MediumPurple', 'MediumSeaGreen', 'MediumSlateBlue', 'MediumSpringGreen',
        'MediumTurquoise', 'MediumVioletRed', 'MintCream', 'MistyRose', 'Moccasin',
        'NavajoWhite', 'OldLace', 'Olive', 'OliveDrab', 'Orange', 'OrangeRed',
        'Orchid', 'PaleGoldenRod', 'PaleGreen', 'PaleTurquoise', 'PaleVioletRed',
        'PapayaWhip', 'PeachPuff', 'Peru', 'Pink', 'Plum', 'PowderBlue', 'Purple',
        'RosyBrown', 'Aquamarine', 'SaddleBrown', 'Green', 'SandyBrown',
        'SeaGreen', 'SeaShell', 'Sienna', 'Silver', 'SkyBlue', 'SlateBlue',
        'SlateGray', 'SlateGrey', 'Snow', 'SpringGreen', 'SteelBlue', 'GreenYellow',
        'Teal', 'Thistle', 'Tomato', 'Turquoise', 'Violet', 'Wheat', 'White',
        'WhiteSmoke', 'Yellow', 'YellowGreen'
    ]
    TEXTALIGN_LEFT = 0
    TEXTALIGN_RIGHT = 1
    colormap=DEFAULT_COLORS

    if clss is None:
        color = colormap[1]
    else:
        color = colormap[int(clss) % len(colormap)]

    draw = ImageDraw.Draw(image)
    im_width, im_height = image.size
    if use_normalized_coordinates:
        (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                      ymin * im_height, ymax * im_height)
    else:
        (left, right, top, bottom) = (xmin, xmax, ymin, ymax)

    if expansion > 0:

        left -= expansion
        right += expansion
        top -= expansion
        bottom += expansion

        # Deliberately trimming to the width of the image only in the case where
        # box expansion is turned on.  There's not an obvious correct behavior here,
        # but the thinking is that if the caller provided an out-of-range bounding
        # box, they meant to do that, but at least in the eyes of the person writing
        # this comment, if you expand a box for visualization reasons, you don't want
        # to end up with part of a box.
        #
        # A slightly more sophisticated might check whether it was in fact the expansion
        # that made this box larger than the image, but this is the case 99.999% of the time
        # here, so that doesn't seem necessary.
        left = max(left,0); right = max(right,0)
        top = max(top,0); bottom = max(bottom,0)

        left = min(left,im_width-1); right = min(right,im_width-1)
        top = min(top,im_height-1); bottom = min(bottom,im_height-1)

    # ...if we need to expand boxes

    draw.line([(left, top), (left, bottom), (right, bottom),
               (right, top), (left, top)], width=thickness, fill=color)

    try:
        font = ImageFont.truetype('arial.ttf', label_font_size)
    except IOError:
        font = ImageFont.load_default()

    # If the total height of the display strings added to the top of the bounding
    # box exceeds the top of the image, stack the strings below the bounding box
    # instead of above.
    display_str_heights = [font.getsize(ds)[1] for ds in display_str_list]

    # Each display_str has a top and bottom margin of 0.05x.
    total_display_str_height = (1 + 2 * 0.05) * sum(display_str_heights)

    if top > total_display_str_height:
        text_bottom = top
    else:
        text_bottom = bottom + total_display_str_height

    # Reverse list and print from bottom to top.
    for display_str in display_str_list[::-1]:

        # Skip empty strings
        if len(display_str) == 0:
            continue

        text_width, text_height = font.getsize(display_str)

        text_left = left

        if textalign == TEXTALIGN_RIGHT:
            text_left = right - text_width

        margin = np.ceil(0.05 * text_height)

        draw.rectangle(
            [(text_left, text_bottom - text_height - 2 * margin), (text_left + text_width,
                                                              text_bottom)],
            fill=color)

        draw.text(
            (text_left + margin, text_bottom - text_height - margin),
            display_str,
            fill='black',
            font=font)

        text_bottom -= (text_height + 2 * margin)
    #image.show()
    return(image)






ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images_dir", required=True,	help="dir where the images are present")
ap.add_argument("-p", "--path_to_model", required=True, help= "path to megadetector_v4_1_0.pb")
ap.add_argument("-pm", "--folder", required=True, help= "where github.com/microsoft/CameraTraps was cloned")
ap.add_argument("-o","--output_path", required=True, help= "path to store the output")
ap.add_argument("-oi","--output_images", required=True, help= "path to store images with bounding boxes")
args = ap.parse_args()
mega_classify()
