import os
import glob

root = os.getcwd()
path_to_inputs = os.path.join(root, "inputs","sub")
output_file_path = os.path.join(root,"evolving_ai_lab",  "val.txt")

if os.path.exists("val.txt"):
    os.remove("val.txt")

paths = []
for p in glob.glob(path_to_inputs + "/*"):
      paths.append(p)
     
print(paths)

with open(output_file_path, 'w') as file:
    for pp in paths:        
        file.write(pp + " " + str(0) + '\n')
