#pip install google-cloud-vision
import numpy as np
import glob
import shutil
from google.cloud import vision
import matplotlib.pyplot as plt
import pandas as pd
from PIL import Image    
import os #path where google json keys are stored
import argparse

root = os.getcwd()


def localize_objects(item):
    """Localize objects in the local image.
    Args:
    path: The path to the local file.
    """
    #i=item
    x=''
    conf="nothing @ 0 @"
    #for data in mega['data']:
    #  if (data==model1['image_path'][i]):
    #    path = data
    path = item
    from google.cloud import vision
    client = vision.ImageAnnotatorClient()

    with open(path, 'rb') as image_file:
        content = image_file.read()
    image = vision.Image(content=content)

    objects = client.object_localization(
        image=image).localized_object_annotations

    no_items = ('{}'.format(len(objects)))
    for object_ in objects:
        conf = ('{}@{}'.format(object_.name, object_.score)) +"@"
        #print('Normalized bounding polygon vertices: ')
        for vertex in object_.bounding_poly.normalized_vertices:
            x = x +"[" +('({},{})'.format(vertex.x, vertex.y)) + "],"
    #return ("GOOGLE CLOUD"+"\n"+"======================="+"\n"+"Prediction = " +conf +no_items +"\n" + "bounding boxes \n" +x)
    return ( conf + str(no_items) +"@" +'{ '+x+' }')


def google_classify(args):
  os.environ["GOOGLE_APPLICATION_CREDENTIALS"]= args.path_to_credentials_json
  path = args.images_dir
  print(path)
  col = ["path","prediction","probability","no_items","bounding boxex"]
  temp=[[]]
  for file in glob.glob(path):
    data = file+"@"+(localize_objects(file))
    x=data.split('@')
    temp.append(x)
  temp.pop(0)
  csv = pd.DataFrame(temp,columns=col)
  csv.to_csv(f'{args.output_path}/google_det.csv')



ap = argparse.ArgumentParser()
ap.add_argument("-i", "--images_dir", required=True,	help="dir where the images are present")
ap.add_argument("-p", "--path_to_credentials_json", required=True, help= "path where the json credentials file was stored")
ap.add_argument("-o","--output_path", required=True, help= "path to store the output")
args = ap.parse_args()
google_classify(args)
